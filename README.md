# Magento Banner Module [![Build Status](https://semaphoreapp.com/api/v1/projects/b09f40a1-d445-46cb-a28f-cba198ab3e31/347893/shields_badge.svg)](https://semaphoreapp.com/jshtylr/banner)

A banner module for Magento

## Features

- Easy to add/edit/delete banners
- Multiple (potentially) transitions
  - Easy to extend own functionality

## Installation

### Modman

You can install this module via modman by copying the directory into the `.modman` directory and running `./modman deploy [directory]`

### Composer

This module can be installed by composer by adding this to your `composer.json`

```
...
"require": {
    ...
    "somarketing/banner": "*",
    ...
},
...
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:somarketing/banner.git"
    }
]
...
```

## Unit Tests

This module provides unit tests (more coverage to come)

### Composer dependencies

```
composer install
```

### Install test version of Magento and EcomDev_PHPUnit module

```
vendor/bin/mage-ci install test-root 1.9.1.0 magento_test -c -t -r http://mage-ci.ecomdev.org  
vendor/bin/mage-ci install-module test-root $(pwd)
vendor/bin/mage-ci shell test-root ecomdev-phpunit.php -a install
vendor/bin/mage-ci shell test-root ecomdev-phpunit.php -a magento-config --db-name magento_test --same-db 1 --base-url http://test.magento.com/
vendor/bin/mage-ci shell test-root ecomdev-phpunit.php -a change-status
```

### Run unit tests

```
chmod +x ./run-tests
./run-tests
```