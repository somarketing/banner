<?php

class So_Banner_Test_Model_Banner extends EcomDev_PHPUnit_Test_Case
{
    public $banner;

    public function setUp()
    {
        parent::setUp();

        $this->banner = Mage::getModel('banner/banner');
    }

    /**
     * @loadFixture banner
     */
    public function testGetActiveBannersNoDate()
    {
        $this->banner->setDateTime('2013-07-17T14:30:00');
        $banners = $this->banner->getActiveBanners();

        $this->assertInstanceOf('Varien_Data_Collection_Db', $banners);
        $data = $banners->getData();

        $this->assertCount(3, $banners);

        $this->assertEquals('Banner 3', $data[2]['title']);
    }

    /**
     * @loadFixture banner
     */
    public function testGetActiveBannersWithDateAfter()
    {
        $this->banner->setDateTime('2013-07-17T14:30:02');
        $banners = $this->banner->getActiveBanners();

        $this->assertInstanceOf('Varien_Data_Collection_Db', $banners);
        $data = $banners->getData();

        $this->assertCount(2, $banners);

        $this->assertEquals('Banner 2', $data[1]['title']);
    }

    /**
     * @loadFixture banner
     */
    public function testGetActiveBannersWithDateBefore()
    {
        $this->banner->setStoreId(1);
        $this->banner->setDateTime('2013-07-17T13:59:59');
        $banners = $this->banner->getActiveBanners();

        $this->assertInstanceOf('Varien_Data_Collection_Db', $banners);
        $data = $banners->getData();

        $this->assertCount(1, $banners);

        $this->assertEquals('Banner 1', $data[0]['title']);
    }

    /**
     * @loadFixture banner
     */
    public function testGetActiveBannersForStore()
    {
        $this->banner->setStoreId(2);
        $this->banner->setDateTime('2013-07-17T23:59:59');
        $banners = $this->banner->getActiveBanners();

        $this->assertInstanceOf('Varien_Data_Collection_Db', $banners);
        $data = $banners->getData();

        $this->assertCount(3, $banners);
        $this->assertEquals('Store 2 Banner', $data[1]['title']);
    }
}
