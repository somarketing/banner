(function($) {
    window.BannerAnimation = function() {
        if (typeof arguments[0] === 'undefined') {
            arguments[0] = {};
        }

        // Setup default arguments
        var defaults = {
            selector: '',
            bannerSelector: '.banner',
            navigationSelector: '.carousel-navigation',
            animateTime: 800,
            animateDelay: 7500
        };
        this.opts = $.extend({}, defaults, arguments[0]);
        this.container = $(this.opts.selector);
        this.banners = this.container.find(this.opts.bannerSelector);
        this.current = 0;
        this.timeout = null;
        this.navigation = $(this.opts.navigationSelector);
        this.navigationItems = this.navigation.find('li');

        var self = this;

        // Ran to start the animation
        this.start = function() {
            this.init();
            this.navigation.on('click', 'a', self.onNavigationClick);
            this.navigationItems.eq(this.current).addClass('active');
            this.timeout = setTimeout(this.onTransition, this.opts.animateDelay);
        };

        // Ran everytime a transition should happen
        this.onTransition = function() {
            clearTimeout(self.timeout);
            self.current++;
            if (self.current === self.banners.length) {
                self.current = 0;
            }

            self.beforeTransition.apply(self);
            self.transition.apply(self, [self.afterTransition]);
            self.timeout = setTimeout(self.onTransition, self.opts.animateDelay);
        };

        // When the user selects a tab
        this.onNavigationClick = function(event) {
            event.preventDefault();
            self.goTo($(this).data('carousel-goto'));
        };
    };

    BannerAnimation.prototype.init = function() {
        this.banners.hide();
        this.banners.eq(0).show();
    };
    BannerAnimation.prototype.beforeTransition = function() {};
    BannerAnimation.prototype.afterTransition = function() {
        this.navigationItems.removeClass('active');
        this.navigationItems.eq(this.current).addClass('active');
    };
    BannerAnimation.prototype.transition = function(callback) {
        callback.apply(this);
    };
    BannerAnimation.prototype.goTo = function(index) {
        clearTimeout(this.timeout);
        this.current = index;
        this.transition(this.afterTransition);
        this.timeout = setTimeout(this.onTransition, this.opts.animateDelay);
    };
}) (jQuery);
