(function($) {
    BannerAnimation.prototype.init = function() {
        var resetStyles = function() {
            this.wrapper.css({
                width: 'auto',
                height: 'auto'
            });
            this.banners.css({
                width: 'auto',
                float: 'none'
            });
        }

        var setWrapperDimensions = function() {
            resetStyles.apply(this);

            this.bannerWidth = this.banners.eq(0).outerWidth(true);
            if (this.container.width() < 1000) {
                this.bannerWidth = this.container.width();
            }

            this.banners.css({
                width: this.bannerWidth,
                float: 'left'
            });
            this.wrapper.width(this.bannerWidth * this.banners.length);
            this.wrapper.height(this.banners.eq(0).outerHeight(true));

            this.wrapper.css({ left: -(this.current * this.bannerWidth) });
        }

        this.banners.wrapAll('<div id="bannersWrapper" />');
        this.wrapper = $('#bannersWrapper');

        // Clone the first to the end, to simulate infinite scrolling
        var first = this.wrapper.find(':first');
        var cloned = first.clone();
        this.wrapper.append(cloned);
        this.banners = this.container.find(this.opts.bannerSelector);

        setWrapperDimensions.apply(this);
        $(window).resize($.proxy(setWrapperDimensions, this));

        this.container.css('overflow', 'hidden');
    }

    BannerAnimation.prototype.transition = function(callback) {
        var self = this;
        var active = this.current === this.banners.length - 1 ? 0 : this.current;

        this.navigationItems.removeClass('active');
        this.navigationItems.eq(active).addClass('active');

        this.wrapper.velocity('stop').velocity({
            left: -(this.current * this.bannerWidth)
        }, {
            duration: this.opts.animateTime,
            complete: function() { callback.apply(self); }
        });
    }

    BannerAnimation.prototype.afterTransition = function() {
        if (this.current === this.banners.length - 1) {
            this.current = 0;
            this.wrapper.css({ left: 0 });
        }
    }
}) (jQuery);
