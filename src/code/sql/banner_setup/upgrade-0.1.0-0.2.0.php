<?php

/** @var So_Banner_Model_Resource_Mysql4_Setup $installer */
$installer = $this;

$installer->getConnection()->addColumn(
    $installer->getTable('banner/banner'),
    'content',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'comment' => 'Banner HTML Content'
    )
);
