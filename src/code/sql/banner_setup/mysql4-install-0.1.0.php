<?php

$installer = $this;
$installer->startSetup();
$installer->run(
    "CREATE TABLE `{$installer->getTable('banner/banner')}` (
      `banner_id` int(11) NOT NULL auto_increment,
      `store_id` varchar(63),
      `title` varchar(100),
      `path` text,
      `link` text,
      `enabled` tinyint(1),
      `date_start` datetime,
      `date_end` datetime,
      `sort_order` smallint(6),
      `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
      PRIMARY KEY (`banner_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);
$installer->endSetup();
