<?php

class So_Banner_Model_Config_Transitions
{
    public function toOptionArray()
    {
        return array(
            array('value' => '0', 'label' => Mage::helper('banner')->__('None')),
            array('value' => 'slide', 'label' => Mage::helper('banner')->__('Slide'))
        );
    }
}
