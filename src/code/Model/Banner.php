<?php

/**
 * Model for Banner table
 *
 * @package Magento
 * @subpackage So_Banner
 */
class So_Banner_Model_Banner extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('banner/banner');
    }

    /**
     * Return the current date/time
     *
     * Unless one is already specified
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        $now = Mage::getModel('core/date')->date();
        if (($date = $this->getData('date_time'))) {
            $now = $date;
        }

        return new \DateTime($now);
    }

    /**
     * Return all active banners for the current set time
     *
     * @return Varien_Data_Collection_Db
     */
    public function getActiveBanners()
    {
        if (!$storeId = $this->getStoreId()) {
            $storeId = 0;
        }

        $now = $this->getDateTime();

        $collection = $this->getCollection();
        $collection->addFieldToFilter('enabled', 1)
            ->addFieldToFilter(
                'store_id',
                array(
                    array('null' => true),
                    array('eq' => 0),
                    array('in' => $storeId)
                )
            )
            ->addFieldToFilter(
                'date_start',
                array(
                    array('null' => true),
                    array('to' => $now->format('c'), 'date' => true)
                )
            )
            ->addFieldToFilter(
                'date_end',
                array(
                    array('null' => true),
                    array('from' => $now->format('c'), 'date' => true)
                )
            )
            ->addOrder('sort_order', Varien_Data_Collection_Db::SORT_ORDER_ASC);
        return $collection;
    }
}
