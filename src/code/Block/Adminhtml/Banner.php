<?php

/**
 * Grid containerfor showing all banners
 *
 * @package Magento
 * @subpackage So_Banner
 */
class So_Banner_Block_Adminhtml_Banner extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Setup the title, controller and block group
     */
    public function __construct()
    {
        $this->_blockGroup = 'banner';
        $this->_controller = 'adminhtml_banner';
        $this->_headerText = $this->__('Manage Banners');

        parent::__construct();
    }
}
