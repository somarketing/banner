<?php

/**
 * Grid for displaying all banners
 *
 * @package Magento
 * @subpackage JT_Banner
 */
class So_Banner_Block_Adminhtml_Banner_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Setup the defaults for the grid
     */
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('sort_order');
        $this->setId('jt_banner_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Return the URI for our collection class
     *
     * @return string
     */
    protected function getCollectionClass()
    {
        return 'banner/banner_collection';
    }

    /**
     * Prepare the collection for the grid
     *
     * @return JT_Banner_Block_Adminhtml_Banner_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->getCollectionClass());
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid|void
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('banner_id');
        $this->getMassactionBlock()->setFormFieldName('banner_id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete', array('' => '')),
            'confirm' => $this->__('Are you sure you want to delete the selected?')
        ));

        $this->getMassactionBlock()->addItem('toggle', array(
            'label' => $this->__('Toggle Enabled'),
            'url' => $this->getUrl('*/*/toggle', array('' => ''))
        ));

        return $this;
    }

    /**
     * Setup the columns for our grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'sort_order',
            array(
                'header' => $this->__('Sort'),
                'index' => 'sort_order',
                'filter' => false,
                'width' => '50px',
                'align' => 'center'
            )
        );

        $this->addColumn(
            'title',
            array(
                'header' => $this->__('Title'),
                'index' => 'title'
            )
        );

        $dateFormat = Mage::app()->getLocale()
            ->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $this->addColumn(
            'date_start',
            array(
                'header' => $this->__('Date Start'),
                'index' => 'date_start',
                'type' => 'date',
                'format' => $dateFormat,
                'time' => true
            )
        );

        $this->addColumn(
            'date_end',
            array(
                'header' => $this->__('Date End'),
                'index' => 'date_end',
                'type' => 'date',
                'format' => $dateFormat,
                'time' => true
            )
        );

        $this->addColumn(
            'path',
            array(
                'header' => $this->__('Image'),
                'index' => 'path',
                'sortable' => false,
                'filter' => false
            )
        );

        $this->addColumn(
            'enabled',
            array(
                'header' => $this->__('Enabled'),
                'index' => 'enabled',
                'width' => '20px',
                'align' => 'center',
                'type' => 'options',
                'options' => array('1' => 'Enabled', '0' => 'Disabled')
            )
        );

        $this->addColumn(
            'store_id',
            array(
                'header' => $this->__('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'filter' => false
            )
        );

        $this->addColumn(
            'actions',
            array(
                'header' => $this->__('Actions'),
                'width' => 15,
                'sortable' => false,
                'filter' => false,
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id',
                        'caption' => $this->__('Edit')
                    )
                )
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
