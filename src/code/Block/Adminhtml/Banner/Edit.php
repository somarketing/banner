<?php

/**
 * Edit form container for a banner
 *
 * @package Magento
 * @subpackage JT_Banner
 */
class So_Banner_Block_Adminhtml_Banner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Setup the form container
     */
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'banner';
        $this->_controller = 'adminhtml_banner';

        parent::__construct();

        $this->setData('form_action_url', $this->getUrl('*/*/save'));

        $this->_updateButton('save', 'label', $this->__('Save Banner'));
        $this->_updateButton('delete', 'label', $this->__('Delete Banner'));
    }

    /**
     * Return the text for the header
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('banner')->getId()) {
            return $this->__('Edit Banner \'%s\'', Mage::registry('banner')->getTitle());
        } else {
            return $this->__('New Banner');
        }
    }
}
