<?php

/**
 * Form block for editing banners
 *
 * @package Magento
 * @subpackage JT_Banner
 */
class So_Banner_Block_Adminhtml_Banner_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Return the label of the tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('banner')->__('Banner Information');
    }

    /**
     * Return the title of the tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('banner')->__('Banner Information');
    }

    /**
     * Return whether the tab can be shown
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Return whether the tab is hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Setup the form fields
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('banner');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'class' => 'fieldset-wide'
            )
        );

        if ($model->getId()) {
            $fieldset->addField(
                'banner_id',
                'hidden',
                array(
                    'name' => 'banner_id',
                )
            );
        }

        $fieldset->addField(
            'title',
            'text',
            array(
                'name' => 'title',
                'label' => $this->__('Title'),
                'title' => $this->__('Title'),
                'required' => true
            )
        );

        $fieldset->addField(
            'link',
            'text',
            array(
                'name' => 'link',
                'label' => $this->__('Link'),
                'title' => $this->__('Link'),
                'required' => false
            )
        );

        $fieldset->addField(
            'date_start',
            'date',
            array(
                'name' => 'date_start',
                'label' => $this->__('Start date'),
                'title' => $this->__('Start date'),
                'format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
                'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'time' => true
            )
        );

        $fieldset->addField(
            'date_end',
            'date',
            array(
                'name' => 'date_end',
                'label' => $this->__('End date'),
                'title' => $this->__('End date'),
                'format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
                'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'time' => true
            )
        );

        $fieldset->addField(
            'enabled',
            'checkbox',
            array(
                'name' => 'enabled',
                'label' => $this->__('Enabled?'),
                'title' => $this->__('Enabled?'),
                'checked' => $model->getEnabled(),
                'onclick' => 'this.value = this.checked ? 1 : 0;'
            )
        );

        $fieldset->addField(
            'sort_order',
            'text',
            array(
                'name' => 'sort_order',
                'label' => $this->__('Sort order'),
                'title' => $this->__('Sort order'),
                'required' => false
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'multiselect',
                array(
                    'name' => 'stores[]',
                    'label' => $this->__('Store View'),
                    'title' => $this->__('Store View'),
                    'required' => true,
                    'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
                )
            );
        } else {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name' => 'stores[]',
                    'value' => Mage::app()->getStore(true)->getId()
                )
            );
        }

        $form->setValues($model->getData());

        $this->setForm($form);

        Mage::dispatchEvent('adminhtml_banner_edit_tab_main_prepare_form', array('form' => $form));

        return parent::_prepareForm();
    }
}
