<?php

class So_Banner_Block_Adminhtml_Banner_Edit_Tab_Content extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function getTabLabel()
    {
        return Mage::helper('banner')->__('Content');
    }

    public function getTabTitle()
    {
        return Mage::helper('banner')->__('Content');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    /**
     * Load Wysiwyg on demand
     *
     * @return Mage_Core_Block_Abstract|void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('banner');

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('banner_');

        $fieldset = $form->addFieldset(
            'image_fieldset',
            array(
                'legend' => Mage::helper('banner')->__('Image'),
                'class' => 'fieldset-wide'
            )
        );

        $fieldset->addField(
            'path',
            'image',
            array(
                'name' => 'path',
                'label' => Mage::helper('banner')->__('Image'),
                'title' => Mage::helper('banner')->__('Image')
            )
        );

        $fieldset = $form->addFieldset(
            'content_fieldset',
            array(
                'legend' => Mage::helper('banner')->__('Content'),
                'class' => 'fieldset-wide'
            )
        );

        $contentField = $fieldset->addField(
            'content',
            'editor',
            array(
                'name' => 'content',
                'style' => 'height:16em',
                'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
                'wysiwyg' => true
            )
        );

        // Remove label column from editor
        $renderer = $this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset_element')
            ->setTemplate('cms/page/edit/form/renderer/content.phtml');
        $contentField->setRenderer($renderer);

        $form->setValues($model->getData());

        $this->setform($form);
    }
}
