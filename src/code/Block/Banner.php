<?php

class So_Banner_Block_Banner extends Mage_Core_Block_Template
{
    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('banners/banner.phtml');
        return parent::_prepareLayout();
    }

    /**
     * Return the processed content for the banner
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->getBanner()->getContent();
    }
}
