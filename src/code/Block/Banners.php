<?php

/**
 * Block template for displaying all of the currently active banners
 *
 * @package Magento
 * @subpackage So_Banner
 */

class So_Banner_Block_Banners extends Mage_Core_Block_Template
{
    /**
     * Caches banner blocks
     *
     * @var array
     */
    protected $_banners = array();

    /**
     * Returns all the currently active banner collection
     *
     * @return So_Banner_Model_Resource_Banner_Collection
     */
    public function getBanners()
    {
        $banner = Mage::getModel('banner/banner');
        $banner->setStoreId(Mage::app()->getStore()->getId());
        return $banner->getActiveBanners();
    }

    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('banners/banners.phtml');
        return parent::_prepareLayout();
    }

    /**
     * Render an individual banner block
     *
     * @param So_Banner_Model_Banner $banner
     * @return mixed
     */
    public function getBannerHtml(So_Banner_Model_Banner $banner)
    {
        $id = $banner->getId();
        if (!isset($this->_banners[$id])) {
            $block = $this->getLayout()
                ->createBlock('banner/banner', 'banner.id_' . $id, array(
                    'banner' => $banner
                ));
            $this->_banners[$id] = $block;
        }
        return $this->_banners[$id]->toHtml();
    }
}
