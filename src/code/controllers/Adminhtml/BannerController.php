<?php

/**
 * Controller for managing the banners
 *
 * @package Magento
 * @subpackage So_Banner
 */
class So_Banner_Adminhtml_BannerController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Initialises some commong things for each action
     */
    protected function initLayout()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/banners')
            ->_title($this->__('CMS'))
            ->_title($this->__('Manage Banners'))
            ->_addBreadcrumb($this->__('CMS'), $this->__('CMS'))
            ->_addBreadcrumb($this->__('Manage Banners'), $this->__('Manage Banners'));
        return $this;
    }

    public function indexAction()
    {
        $this->initLayout();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->initLayout();

        // Get id if available
        $id =  $this->getRequest()->getParam('id');
        $model = Mage::getModel('banner/banner');

        if ($id) {
            $model->load($id);

            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('Banner doesn\'t exist'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Banner'));

        $data = Mage::getSingleton('adminhtml/session')->getBannerData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('banner', $model);

        $this->_addBreadcrumb(
            $id ? $this->__('Edit Banner') : $this->__('New Banner'),
            $id ? $this->__('Edit Banner') : $this->__('New Banner')
        );
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('banner/banner');

            // Handle image uploads/deletion
            $image = null;
            if (isset($_FILES['path']['name']) && $_FILES['path']['name'] != '') {
                $image = $this->uploadImage();
            } else {
                if (isset($postData['path']['delete']) && $postData['path']['delete'] == 1) {
                    $image = null;
                }
                if (isset($postData['path']['value'])) {
                    $postData['path'] = $postData['path']['value'];
                }
            }
            if ($image) {
                $postData['path'] = $image;
            }

            // Set whether the banner is enabled/disabled
            if (!isset($postData['enabled'])) {
                $postData['enabled'] = 0;
            }

            // Check for store data
            if (isset($postData['stores'])) {
                if (in_array('0', $postData['stores'])) {
                    $postData['store_id'] = 0;
                } else {
                    $postData['store_id'] = implode(",", $postData['stores']);
                }
                unset($postData['stores']);
            }

            $model->setData($postData);

            // Validate the data
            if (!$this->_validatePostData($postData)) {
                $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current' => true));
                return;
            }

            // Attempt to save our model
            try {
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The banner has been saved.'));
                $this->_redirect('*/*/');
                return;
            }
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError('An error occurred while saving this banner.');
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/', array('id' => $this->getRequest()->getParam('id')));
            return;
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $model = Mage::getModel('banner/banner');
            $model->setId($id);
            $model->delete();

            Mage::getSingleton('adminhtml/session')
                ->addSuccess($this->__('The banner has been deleted'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')
                ->addError($e->getMessage());
        }

        $this->_redirect('*/*');
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('banner_id');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')
                ->addError($this->__('Please select some banners'));
        } else {
            try {
                $model = Mage::getModel('banner/banner');
                foreach ($ids as $id) {
                    $model->setId($id)->delete();
                }
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess($this->__('Total of %d record(s) were deleted.', count($ids)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')
                    ->addError($this->__($e->getMessage()));
            }
        }
        $this->_redirect('*/*/index');
    }

    public function toggleAction()
    {
        $ids = $this->getRequest()->getParam('banner_id');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')
                ->addError($this->__('Please select some banners'));
        } else {
            try {
                $model = Mage::getModel('banner/banner');
                foreach ($ids as $id) {
                    $model->load($id);
                    $model->setEnabled(!$model->getEnabled());
                    $model->save();
                }
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess($this->__('Total of %d record(s) were modified.', count($ids)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')
                    ->addError($this->__($e->getMessage()));
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Uploads the image from the formdata
     */
    protected function uploadImage()
    {
        $uploader = new Varien_File_Uploader('path');

        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);

        $dir = Mage::getBaseDir('media') . DS . 'banners' . DS;
        $filename = $_FILES['path']['name'];

        $path = pathinfo($filename);
        $origFilename = $path['filename'];
        while (file_exists($dir . $filename)) {
            $pieces = array();
            $path = pathinfo($filename);
            $res = preg_match('/^(.+)_(\d+)$/', $path['filename'], $pieces);

            if (!$res) {
                $suffix = '_1';
            } else {
                $suffix = '_' . strval(intval($pieces[2]) + 1);
            }
            $filename = $origFilename . $suffix . '.' . $path['extension'];
        }

        $uploader->save($dir, $filename);
        return Mage::getBaseUrl('media') . 'banners' . DS . $filename;
    }

    protected function _validatePostData($data)
    {
        $errorNo = true;
        if (!empty($data['layout_update_xml'])) {
            $validatorCustomLayout = Mage::getModel('adminhtml/layoutUpdate_validator');
            if (!$validatorCustomLayout->isValid($data['layout_update_xml'])) {
                $errorNo = false;
            }
            foreach ($validatorCustomLayout->getMessages() as $message) {
                $this->_getSession()->addError($message);
            }
        }
        return $errorNo;
    }
}
