<?php

class So_Banner_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_bannerJsDir = 'js/banner';

    /**
     * Returns the name of the javascript file to include.
     *
     * @return string
     */
    public function getScriptFilename()
    {
        $filename = '';
        switch (Mage::getStoreConfig('cms/banner_options/transition')) {
            case 'slide':
                $filename = 'slide';
                break;
            default:
                return false;
        }

        return $this->_bannerJsDir . '/transition/' . $filename . '.js';
    }
}
